pipeline {
    agent any 
    parameters {
        string(name:'TAG_NAME',defaultValue: 'latest',description:'')
    }
    environment {
        APP_NAME = 'CompassChecker'
    }
    stages {
        stage('Init'){
            steps{
                script{
                    sh 'mkdir -p src/$APP_NAME'
                    sh 'mv config.json go.mod go.sum main.go src/$APP_NAME'
                }
            }
        }
        stage('Golang-linter') {
            steps {
                script{
                    try{
                        def root = tool name: 'go1.14', type: 'go'
                        withEnv(["GOPATH=${WORKSPACE}", "PATH+GO=${root}/bin:${WORKSPACE}/bin", "GOBIN=${WORKSPACE}/bin"]){
                            dir("${env.WORKSPACE}/src/$APP_NAME"){
                                sh "echo \"199.232.4.133 raw.githubusercontent.com\" | tee -a /etc/hosts"
                                sh "echo \"run:\nmodules-download-mode: \"readonly\"\noutput:\n    format: colored-line-number\n    uniq-by-line: false\nlinters:\n    disable-all: true\n    enable:\n        # unused code\n        - structcheck\n        - ineffassign\n        - unused\n        - deadcode\n        # code style\n        - misspell\n        - goconst\n        - unconvert\n        - stylecheck\n        - lll\n        - errcheck\n        - wsl\n        - gofmt\n        - goimports\n        - gosimple\n        # complexity\n        - gocyclo\n        - nakedret\n        # bugs\n        - scopelint\n        - staticcheck\nlinters-settings:\n    # code style\n    misspell:\n        locale: US\n    goconst:\n        min-occurrences: 2\n    lll:\n        line-length: 120\n        tab-width: 4\n    errcheck:\n        check-type-assertions: true\n        check-blank: true\n    # complexity\n    gocyclo:\n        min-complexity: 15\n    nakedret:\n        max-func-lines: 2\">>.golangci.yml"
                                sh "curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b \$(go env GOPATH)/bin v1.24.0"
                                sh "go env -w GO111MODULE=on;go env -w GOPROXY=https://goproxy.cn,direct"
                                sh "go get golang.org/x/tools/cmd/goimports"
                                sh "gofmt -w main.go"
                                sh "goimports -w main.go"
                                sh "golangci-lint run -v ."
                            }
                        }
                    }
                    catch(all){
                        deleteDir()
                        error('Failed to build')
                    }
                }
            }
        }
        stage('Build') {
            steps {
                script{
                    try{
                        def root = tool name: 'go1.14', type: 'go'
                        withEnv(["GOPATH=${WORKSPACE}", "PATH+GO=${root}/bin:${WORKSPACE}/bin", "GOBIN=${WORKSPACE}/bin"]){
                            dir("${env.WORKSPACE}/src/$APP_NAME"){
                                sh "go env -w GO111MODULE=on;go env -w GOPROXY=https://goproxy.cn,direct"
                                sh "go get -v"
                                sh 'CGO_ENABLED=0 GOOS=linux GOARCH=arm64 go build -v -a -ldflags -w -installsuffix cgo -o ${APP_NAME}_arm .'
                                sh 'CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -v -a -ldflags -w -installsuffix cgo -o ${APP_NAME}_amd .'
                            }
                        }
                    }
                    catch(all){
                        deleteDir()
                        error('Failed to build')
                    }
                }
            }
        }
        stage('Archive'){
            steps{
                script{
                    dir("${env.WORKSPACE}/src/$APP_NAME"){
                        sh "tar -zcvf ${APP_NAME}_${params.TAG_NAME}_Linux_arm64.tar.gz config.json ${APP_NAME}_arm && tar -zcvf ${APP_NAME}_${params.TAG_NAME}_Linux_amd64.tar.gz config.json ${APP_NAME}_amd"
                        sh 'echo  \'#!/bin/sh\ntoolName=APP_NAME\nSHELL_FOLDER=$(cd \"$(dirname \"\$0\")\";pwd)\nif [ ! -d \"\$toolName\" ]; then\n    mkdir \$toolName\nfi\ncurl -LJ https://console.uisee.com/compass-checker/\${toolName}_latest_Linux_arm64.tar.gz | tar -zx -C \$toolName\nchmod +x \$toolName/\${toolName}_arm\ncd \$SHELL_FOLDER/\$toolName\n./\${toolName}_arm\'>deploy_arm.sh'
                        sh 'echo  \'#!/bin/sh\ntoolName=APP_NAME\nSHELL_FOLDER=$(cd \"$(dirname \"\$0\")\";pwd)\nif [ ! -d \"\$toolName\" ]; then\n    mkdir \$toolName\nfi\ncurl -LJ https://console.uisee.com/compass-checker/\${toolName}_latest_Linux_amd64.tar.gz | tar -zx -C \$toolName\nchmod +x \$toolName/\${toolName}_arm\ncd \$SHELL_FOLDER/\$toolName\n./\${toolName}_amd\'>deploy_amd.sh'
                        sh 'sed -i \'s/APP_NAME/\'\"${APP_NAME}\"\'/\' deploy_arm.sh deploy_amd.sh'
                        sh 'sed -i \'s/latest/\'\"${TAG_NAME}\"\'/\' deploy_arm.sh deploy_amd.sh'
                        archiveArtifacts '*.tar.gz,*.sh'
                    }
                }
            }
        }
        //stage('deleteDir'){
        //    steps{
        //        script{
        //            deleteDir()
        //        }
        //    }
        //}
    }
    post{
       success{
           dir("${env.WORKSPACE}/src/$APP_NAME"){
                sh 'ls -l'
                step([$class: 'MinioUploader',sourceFile: '*.tar.gz,*.sh', bucketName: 'compass-checker'])
                deleteDir()
            }
        }
    }
}